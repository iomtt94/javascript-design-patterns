// function Server(name, ip) {
//     this.name = name;
//     this.ip = ip;
// }

// Server.prototype.getUrl = function() {
//     return `Server ip address is https://${this.ip}:80 and name is ${this.name}`;
// }

// let github = new Server('github', '192.168.1.1');
// console.log(github.getUrl());


class Server {
    constructor(name, ip) {
        this.name = name;
        this.ip = ip;
    }

    getUrl() {
        return `Server ip address is https://${this.ip}:80 and name is ${this.name}`;
    }
}

let bitbucket = new Server('bitbucket', '192.168.1.1');

console.log(bitbucket.getUrl());