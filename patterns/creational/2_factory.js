class SimpleAccess {
    constructor(name) {
        this.name = name;
        this.cost = 50;
    }
}

class StandartAccess {
    constructor(name) {
        this.name = name;
        this.cost = 150;
    }
}

class PremiumAccess {
    constructor(name) {
        this.name = name;
        this.cost = 300;
    }
}

class MemberFactory {
    static #list = {
        simple: SimpleAccess,
        standart: StandartAccess,
        premium: PremiumAccess
    };

    userNumber = 0;

    create(name = `defaultUser${this.userNumber++}`, type = SimpleAccess) {
        switch(type) {
            case MemberFactory.#list.simple:
                return new SimpleAccess(name);
            case MemberFactory.#list.standart:
                return new StandartAccess(name);
            case MemberFactory.#list.premium:
                return new PremiumAccess(name);
            default:
                console.log(`Sorry, we are out of types.`);
        }
    }
}

const factory = new MemberFactory();

var members = [
    factory.create('User1', SimpleAccess),
    factory.create('User2', StandartAccess),
    factory.create('User3', PremiumAccess)
];

console.log(members); 

/* result:
[
    SimpleAccess { name: 'User1', cost: 50 },
    StandartAccess { name: 'User2', cost: 150 },
    PremiumAccess { name: 'User3', cost: 300 }
]
*/